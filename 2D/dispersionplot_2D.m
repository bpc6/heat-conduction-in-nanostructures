%{
Generates surface plots for every branch in each displacement family
for the 2D case (x and y displacement). Also gives the ky=0 case plot
and generates a movie if desired.
%}
function dispersionplot_2D(N, t, kx, ky, omega_u, omega_v, movie)
%% 2D Plot
E_u = omega_to_E(omega_u); % meV
E_v = omega_to_E(omega_v); % meV

% Plot the case for ky=0, this should match J. Zou
figure(1), clf, hold on, box on
U = plot(kx/1e9, E_u(1:N+1, :, 1), 'color', '#0072BD');
V = plot(kx/1e9, E_v(1:N+1, :, 1), 'color', '#D95319');
xlabel('k_x (nm^{-1})')
ylabel('Phonon Energy (meV)')
title('2D dispersion for k_y = 0')
legend([U(1) V(1)], {'u-family', 'v-family'}, 'location', 'nw');

%% Surface Plots
subw = ceil(sqrt(N+1));  % The # of subplots along the width of fig
subh = ceil((N+1) / subw); % The # of subplots along height of fig

% (x) Branch Surface Plots:
figure(2), sgtitle(sprintf('2D u-family'))
for kz = 1:N+1
    subplot(subh, subw, kz)
    surf(kx/1e9, ky/1e9, permute(E_u(kz,:,:), [3 2 1]))
    xlabel('k_x')
    ylabel('k_y')
    zlabel('Phonon Energy (meV)')
    title(sprintf('kz = %1.2f nm^{-1}', (kz-1)*pi/t))
end

% (v) Branch Surface Plots
figure(3), sgtitle(sprintf('2D v-family'))
for kz = 1:N+1
    subplot(subh, subw, kz)
    surf(kx/1e9, ky/1e9, permute(E_v(kz,:,:), [3 2 1]))
    xlabel('k_x')
    ylabel('k_y')
    zlabel('Phonon Energy (meV)')
    title(sprintf('kz = %1.2f nm^{-1}', (kz-1)*pi/t))
end

%% Make a movie
if movie
    figure(5)
    for index = 1:yres
        clf, hold on, box on
        U = plot(kx/1e9, E_u(1:N+1, :, index), 'color', '#0072BD');
        V = plot(kx/1e9, E_v(1:N+1, :, index), 'color', '#D95319');
        xlabel('k_x (nm^{-1})')
        ylabel('Phonon Energy (meV)')
        ylim([0 80])
        title(sprintf('2D dispersion for k_y = %3.2f nm^{-1}', ...
            ky(index)/1e9))
        legend([U(1) V(1)], {'u branch', 'v branch'}, 'location', 'nw');
        set(gcf, 'position', [100, 100, 1500, 900]);
        drawnow
        movieVector(index) = getframe(gcf);
    end
    
    writer = VideoWriter('./2D/dispersion_2D_changing_ky', 'MPEG-4');
    writer.FrameRate = 3;
    
    open(writer);
    writeVideo(writer, movieVector);
    close(writer);
end
end