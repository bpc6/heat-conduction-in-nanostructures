%{
Take a set of eigenvalues and eigenvectors and sort them according to
our custom algorithm to seperate dispersion eigenvalues into their
curve families.
%}
function [omega_u, omega_v] = eigensort_2D(V,D,omega_u,omega_v,kx,ky,i,j)
[d,ind] = sort(diag(D));  % sort in increasing value
zres = length(d)/2;
Vs = V(:,ind); % Reorganize eigenvectors to match sorted eigenvalues

% Resort eigenvalues into u and v based on the corresponding eigenvectors
u_count = 1;
v_count = 1;
for k = 1:2*zres
    %{
    The u-component of eigenvector V is the first (zres) elements of V
    and the v-component is the second (zres) elements of V
    %}
    u = Vs(1:zres,k);
    v = Vs(zres+1:end,k);
    om = real(sqrt(d(k)));  % get all the omega values from eigenvalues
    
    %{
    For nonzero kx and ky, if the u and v components have the same sign,
    they belong to a u-eigenvalue (Longitudinal). If they are inverted,
    they belong to a v-eigenvalue (Transverse). There are 4 edge cases
    when kx or ky is nonzero which can be identified by comparing the
    amplitude of the components and checking which is greater, kx or ky.
    %}
    if u(1)*v(1) > 0
        omega_u(u_count,i,j) = om;
        u_count = u_count+1;
    elseif u(1)*v(1) < 0
        omega_v(v_count,i,j) = om;
        v_count = v_count+1;
        % Cover the edge cases
    elseif mean(abs(u)) > mean(abs(v)) && kx(i) >= ky(j)
        omega_u(u_count,i,j) = om;
        u_count = u_count+1;
    elseif mean(abs(u)) < mean(abs(v)) && kx(i) >= ky(j)
        omega_v(v_count,i,j) = om;
        v_count = v_count+1;
    elseif mean(abs(u)) > mean(abs(v)) && kx(i) < ky(j)
        omega_v(v_count,i,j) = om;
        v_count = v_count+1;
    else
        omega_u(u_count,i,j) = om;
        u_count = u_count+1;
    end
end
end