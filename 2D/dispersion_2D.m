%{
Computes the phonon dispersion of any structure by setting up and solving
the eigenvalue problem [A]x=l[B]x. Simulation is for particle displacement
in x and y and wave propagation in any direction

@param thicknesses: The thickness vector of the heterostructure, in m
@param materials: The material vector, given as strings. Ex: ["GaN" "AlN"]
@param xres: The resolution in the kx direction
@param yres: The resolution in the ky direction
@param zres: The resolution in the kz direction, aka computation resolution
@return N: The number of phonon branches
@return t: The total thickness of the heterostructure
@return kx: A vector representation of kx
@return ky: A vector representation of ky
@return omega_u: The u-displacment family of phonon dispersion branches
@return omega_v: The v-displacment family of phonon dispersion branches
%}
function [N, t, kx, ky, omega_u, omega_v] = dispersion_2D(...
    thicknesses, materials, xres, yres, zres)

%% Build heterostructure and set up eigenvalue problem
if length(thicknesses) ~= length(materials)
    error('The length of thicknesses and materials must match')
end

[C, rho, a, c, t, h] = build_heterostructure(materials, thicknesses, zres);
N = round(t / (2*c)); % ND
omega_u = zeros(zres, xres, yres);
omega_v = zeros(zres, xres, yres);
kx = linspace(0, pi/a, xres);
ky = linspace(0, pi/a, yres);

%% Solve eigenvalue problem
for j = 1:yres
    for i = 1:xres
        A = build_A_2D(kx(i), ky(j), C, h, zres);
        B = diag([rho rho]);
        [VV,D] = eig(A,B);  % compute eigenvalues
        % sort the eigenvalues into their curve families:
        [omega_u, omega_v] = eigensort_2D(VV,D,omega_u,omega_v,kx,ky,i,j);
    end
    fprintf('j = %d/%d\n', j, yres)
end
end