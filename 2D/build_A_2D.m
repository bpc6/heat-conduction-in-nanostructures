%{
Build the matrix A for solving the eigenvalue problem [A]x=l[B]x

@param kx: Vector form of the x component of the wavevector
@param ky: Vector form of the y component of the wavevector
@param C: Vectorized form of the elastic coeficient matrix.
    Shape: (zres)x6x6
@param h: distance between nodes
@param zres: Number of discrete nodes in the z-direction
@return A: A matrix representation of the 1D continuum elastic wave
    equation
%}
function A = build_A_2D(kx, ky, C, h, zres)
[p,q,r,s,t,u,pp,qq,rr,ss,tt,uu] = deal(zeros(length(C),1));

for i = 1:length(C)
    % Apply boundary conditions
    im1 = i - 1;
    ip1 = i + 1;
    if i == 1
        im1 = 2;
    end
    if i == length(C)
        ip1 = length(C) - 1;
    end
    
    % Assign the diagonal vectors (see math for derivations)
    p(i) = C(i,1,1)*kx^2 + C(i,1,6)*kx*ky + C(i,6,1)*kx*ky + ...
        C(i,6,6)*ky^2 + C(i,5,5)*2/h^2;
    q(i) = (C(ip1,5,5) - C(im1,5,5))/(4*h^2) - C(i,5,5)/h^2;
    r(i) = -(C(ip1,5,5) - C(im1,5,5))/(4*h^2) - C(i,5,5)/h^2;
    s(i) = C(i,1,2)*kx*ky + C(i,1,6)*kx^2 + C(i,6,2)*ky^2 + ...
        C(i,6,6)*kx*ky + C(i,5,4)*2/h^2;
    t(i) = (C(ip1,5,4) - C(im1,5,4))/(4*h^2) - C(i,5,4)/h^2;
    u(i) = -(C(ip1,5,4) - C(im1,5,4))/(4*h^2) - C(i,5,4)/h^2;
    
    pp(i) = C(i,6,1)*kx^2 + C(i,6,6)*kx*ky + C(i,2,1)*kx*ky + ...
        C(i,2,6)*ky^2 + C(i,4,5)*2/h^2;
    qq(i) = (C(ip1,4,5)-C(im1,4,5))/(4*h^2) - C(i,4,5)/h^2;
    rr(i) = -(C(ip1,4,5)-C(im1,4,5))/(4*h^2) - C(i,4,5)/h^2;
    ss(i) = C(i,6,2)*kx*ky + C(i,6,6)*kx^2 + C(i,2,2)*ky^2 + ...
        C(i,2,6)*kx*ky + C(i,4,4)*2/h^2;
    tt(i) = (C(ip1,4,4)-C(im1,4,4))/(4*h^2) - C(i,4,4)/h^2;
    uu(i) = -(C(ip1,4,4)-C(im1,4,4))/(4*h^2) - C(i,4,4)/h^2;
end

% Build the matrix' 4 sub matrices and the diagonals above
diags = [q p r];
A1 = full(spdiags(diags, -1:1, zres, zres));
A1(1,2) = q(1)+r(1);
A1(end, end-1) = q(end)+r(end);
diags = [t s u];
% A2 = full(spdiags(diags, -1:1, zres, zres));
A2 = full(spdiags(diags, -1:1, zres, zres));
A2(1,2) = t(1)+u(1);
A2(end, end-1) = t(end)+u(end);
diags = [qq pp rr];
% A3 = full(spdiags(diags, -1:1, zres, zres));
A3 = full(spdiags(diags, -1:1, zres, zres));
A3(1,2) = qq(1)+rr(1);
A3(end, end-1) = qq(end)+rr(end);
diags = [tt ss uu];
% A4 = full(spdiags(diags, -1:1, zres, zres));
A4 = full(spdiags(diags, -1:1, zres, zres));
A4(1,2) = tt(1)+uu(1);
A4(end, end-1) = tt(end)+uu(end);
A = [A1 A2; A3 A4];
end