clear
config;

[N, t, kx, ky, omega_u, omega_v] = dispersion_2D(...
    thicknesses, materials, xres, yres, zres);

dispersionplot_2D(N, t*1e9, kx, ky, omega_u, omega_v, movie)