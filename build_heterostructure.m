%{
From the user's input, build the required material property vectors

@param materials: an array of materials with format:
    ["Material1", "Material2", ...]
    Ex: ["AlN", "GaN", "AlN"]
@param thicknesses: an array of thicknesses (in nm) associated with each
material in materials. Must be the same length.
    Ex: [2 6 2] * 1e-9
@param zres: the computational resolution in z
@return C: 3D matrix {zres, 6, 6} giving the elastic coefficient matrix for
each node in the heterostructure
@return rho: vector {zres} giving the density for each node in the
heterostructure
@return N: Number of modes to be plottted in kz
@return h: distance between nodes in z
%}
function [C, rho, a, c, t, h] = build_heterostructure(materials, ...
    thicknesses, zres)
% This is really slow but builds C and rho for the heterostructure
t = sum(thicknesses); % m
if zres > 1
    h = t/(zres-1); % m
else
    h = 1e10; % a big number
end

for prop_index = 1:length(materials)
    filename = sprintf('./properties/%s.mat', materials(prop_index));
    try
        props(prop_index) = load(filename);
    catch ME
        error("We don't have the material: %s", materials(prop_index));
    end
end

C = zeros(zres,6,6);
rho = zeros(1,zres);
a_vec = zeros(1,zres);
c_vec = zeros(1,zres);
index = 1;
for thickness_index = 1:length(thicknesses)
    upto = sum(thicknesses(1:thickness_index));
    while (index-1)*h <= upto
        C(index,:,:) = props(thickness_index).C;
        rho(index) = props(thickness_index).rho;
        a_vec(index) = props(thickness_index).a;
        c_vec(index) = props(thickness_index).c;
        index = index + 1;
    end
end
a = mean(a_vec);
c = mean(c_vec);
end