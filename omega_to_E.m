%{
Converts a scalar or matrix angular frequency to phonon energy.
%}
function E = omega_to_E(omega)
E = omega * hbar / eV * 1e3; % meV
end