function h = planck  % Planck's constant.
h = 6.62617e-34; %m2-kg/s (J*s)
end

% From Razeghi, M., Fundamentals of Solid State Engineering