function h = hbar % Reduced Planck's constant
h = planck / (2*pi); % J*s
end

% From Razeghi, M., Fundamentals of Solid State Engineering