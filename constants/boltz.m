function kb = boltz % Boltzmann constant
kb = 1.38064852e-23; % m^2-kg/s^2-K
end