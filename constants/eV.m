function e = eV % electron Volt definition
e = 1.60218e-19; %J
end

% From Razeghi, M., Fundamentals of Solid State Engineering