# GPD: Generalized Phonon Dispersion for Analysis of Nanostructure Applications

## 1. Introduction
GPD is a set of codes that compute the phonon dispersion relation and additional thermal properties of a user defined nanostructure, whether homogenous or heterogenous in structure. For more detail concerning the principles of phonon and the justification for the model see our corresponding paper. This manual explains how to use the code and provides commentary on the logic and algorithms used so that the code can be edited and improved for future applications.
In general, all units used in the code are SI, meaning that all values are defined using SI units: kilograms for mass, meters for length, seconds for time, and Kelvin for temperature.

## 2. Setup
### 2.1 Download
The GPD source code is available for public download. It was written in Matlab version 2020a, so this version or later is recommended. The source code can be cloned or downloaded as a .zip file from the GitLab-based source control site https://code.vt.edu/bpc6/heat-conduction-in-nanostructures. If downloaded as a .zip, the folder must be extracted before use.
### 2.2 Project File
GPD is a series of codes organized as a Matlab Project. More information on Matlab Projects can be found at https://www.mathworks.com/help/matlab/projects.html. All code should be run from the Matlab GUI. Inside the root directory, open the main project file, `HeatConductionNanosemiconductors.prj`, before running any of the files. The project file manages file dependency and some files will not be found if the project file is not running, therefore it must be started every time before running any other codes. All future discussion in this manual assumes that the project is running.
### 2.3 Code Architecture
The code is organized into the following folders:
* `1D`
* `2D`
* `3D`
* `constants`
* `properties`
* `Validation`

`1D`, `2D`, and `3D` contain the source code for running the simulations in their respective dimensions and will be addressed in sections 3 & 4 in this document. `constants` contains Matlab functions that define the following global constants for the project: 
* `boltz` - Boltzmann’s constant
* `eV` - The conversion factor from Joules to electron volts.
* `hbar` - Reduced Planck’s constant
* `planck` - Planck’s constant

`properties` contains a library of material properties that are required for phonon dispersion simulations. Gallium nitride (GaN), aluminum nitride (AlN), and silicon (Si) come already prepared in the base download, but more files can be added (see section 2.4). Validation contains files that were used to evaluate the accuracy of and runtime of the code. It is for development purposes only. Lastly, there are a few files in the root folder that are used for simulations across the 1D, 2D, and 3D directories.
### 2.4 Adding Materials to the Properties Library
As mentioned in section 2.3, the `properties` folder contains the materials library, along with a function to add new materials into the library. To add a new material, run `add_material()` from the Matlab command window. Remember that all numerical values are taken in SI units. The material being added is identified by the first input, `filename`. This parameter gives the name of a file that defines the 6x6 elastic coefficient matrix corresponding to that material. Therefore, to add gallium arsenide to the library, a `.txt`, `.csv`, or `.xlsx` would be prepared called `GaAs.csv` (depending on the extension), which contains a 6x6 coefficient matrix, where all the elements are in Pa (SI units). The other properties are defined directly on the command line when the code is called. After adding the material, it will be stored in the library for future use, so this process must only occur once per material.

## For Complete Documentation
For the rest of the documentation, including the equations and algrithms used, please see see `user-manual.pdf` and `GPD_Final_Paper.pdf` located in the root directory.
