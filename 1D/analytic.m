%{
Computes the phonon dispersion of a thin film using the analytically
    derived solution. Simulation is for particle displacement in y and
    wave propagation in x and z

@param thickness: The thickness of the thin film, in m
@param material: The material given as a string. Ex: "GaN"
@param xres: The resolution in the kx direction
@return N: The number of phonon branches
@return kx: A vector representation of kx
@return omega_v: The v-displacment family of phonon dispersion branches
%}
function [N, kx, omega_v] = analytic(thickness, material, xres)
if length(thickness) ~= 1
    error("Analytic solution requires a thin film!")
end
[C, rho, a, c, t, ~] = build_heterostructure(material, thickness, 1);
C = permute(C, [2, 3, 1]);
N = round(t / (2*c)); % ND

kx = linspace(0, pi/a, xres); % rad/m
kz = (0:N)*pi/t; % rad/m

[KX,KZ] = meshgrid(kx,kz);
omega_v = sqrt((C(6,6)*KX.^2+C(4,4)*KZ.^2)/rho); % rad/s
end