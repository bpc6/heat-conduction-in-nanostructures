%{
Generate a plot of the phonon dispersion
%}
function dispersionplot_1D(N, kx, omega_v, fignum)
E_v = omega_to_E(omega_v); % meV

figure(fignum), clf, hold on, box on
plot(kx/1e9, E_v(1,:), 'k--')
plot(kx/1e9, E_v(2:N+1,:), 'k')
hold off
xlabel('k_x (nm^{-1})')
ylabel('Phonon Energy (meV)')
title('Displacement in y only, k_y = 0')
end