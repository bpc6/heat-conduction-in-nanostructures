%{
Computes the phonon dispersion of any structure by setting up and solving
the eigenvalue problem [A]x=l[B]x. Simulation is for particle displacement
in y and wave propagation in x and z

@param thicknesses: The thickness vector of the heterostructure, in m
@param materials: The material vector, given as strings. Ex: ["GaN" "AlN"]
@param xres: The resolution in the kx direction
@param zres: The resolution in the kz direction, aka computation resolution
@return N: The number of phonon branches
@return t: The total thickness of the heterostructure
@return kx: A vector representation of kx
@return omega_v: The v-displacment family of phonon dispersion branches
%}
function [N, t, kx, omega_v] = dispersion_1D(...
    thicknesses, materials, xres, zres)

%% Build heterostructure and set up eigenvalue problem
if length(thicknesses) ~= length(materials)
    error('The length of thicknesses and materials must match')
end

[C, rho, a, c, t, h] = build_heterostructure(materials, thicknesses, zres);
N = round(t / (2*c)); % ND
omega_v = zeros(zres, xres);
kx = linspace(0, pi/a, xres);

%% Solve eigenvalue problem
for i = 1:xres
    A = build_A_1D(kx(i), C, h, zres);
    B = diag(rho);
    omega_v(:, i) = real(sqrt(sort(eig(A,B))));
end
end