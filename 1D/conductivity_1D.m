%{
Compute the thermal conductivity according to the method in
J. Appl. Phys. 100, 104309 2006.

@param kx: wavevector in the x-direction. Shape: (1,xres)
@param omega: Angular frequency from 1D phonon dispersion. Shape: (N,xres)
    NOTE: This should NOT include the first mode from the dispersion. Ex:
    omega(2,N+1)
@param t: structure thickness, in m
@param T: operation temperature, in K
@param material_name: name of the material for which thermal conductivity
    will be computed, given as a string. Ex: "GaN" or "Si"
%}
function [kappa, ratio] = conductivity_1D(kx, omega, t, T, material_name)
N = size(omega, 1);
qlength = 50000;  % sets the resolution for interpolation. This value
                  % shouldn't need to be changed
interpOmega = zeros(N, qlength);
kx_query = linspace(kx(1), kx(end), qlength);
for n = 1:N  % fill out interpolated version of omega curves
    interpOmega(n,:) = interp1(kx, omega(n,:), kx_query);
end
omega_vec = linspace(interpOmega(1), interpOmega(end), qlength); % makes a
    % vector ranging from the min to max omega. For plotting.

[Vbar, V] = avg_velo(kx, omega, omega_vec);
figure(3), clf, hold on, box on
plot(omega_to_E(omega_vec), Vbar/1e3, 'k')
xlabel('Phonon Energy (meV)')
ylabel('Group Velocity (km/s)')
title('Average Group Velocity')

g = density_states(kx, omega, omega_vec, V, t);
figure(4), clf, hold on, box on
plot(omega_to_E(omega_vec), g/1e14, 'k')
xlabel('Phonon Energy (meV)')
ylabel('Density of States (10^{14}) s/m^3')
title('Density of States')

% Get necessary material properties for density of states
filename = sprintf('./properties/%s', material_name);
material = load(filename);
V0 = material.vol;
rho = material.rho;
gamma = material.gamma;
Tdebye = material.Tdebye;

tao = relaxation(interpOmega, gamma, T, V0, Tdebye, Vbar, rho);
figure(5), clf, hold on, box on
plot(omega_to_E(omega_vec), tao*1e9)
xlabel('Phonon Energy (meV)')
ylabel('Relaxation Time (ms)')
labels = strings(size(omega,1), 1);
for n = 1:size(omega,1) % make a nice legend
    labels(n) = sprintf('n = %d', n);
end
legend(labels)
title('Relaxation TIme')

% ------------------------------------------------------------------
% Now that we have nice plots, compute Vbar, g, and tao again using x
% as defined in Eq. 11 for actual computation
x = xfunction(omega, T);
x_vec = xfunction(omega_vec, T);
[Vbar, V] = avg_velo(kx, x, x_vec);
g = density_states(kx, x, x_vec, V, t);
tao = relaxation(xfunction(interpOmega, T), gamma, T, V0, 600, Vbar, rho);

% Equation 11:
integrand = x_vec.^2 .* exp(x_vec) .* (exp(x_vec)-1).^-2 ...
    .* tao .* Vbar.^2 .* g;
kappa = 1/3 * boltz/hbar * boltz * T * sum(trapz(omega_vec, integrand, 2));
ratio = kappa / 200;
end


%{
Compute the Relaxation Time according to Equation 13.

@param interpOmega: interpolated version of omega. Size: (N,50000)
@param gamma: Gruneisen anharmonicity parameter (ND)
@param T: operational Temperature, in K
@param V0: volumer per atom
@param Tdebye: Debye Temperature, in K
@param Vbar: Average group velocity. Size: (1,50000)
@param rho: density
%}
function tao = relaxation(interpOmega, gamma, T, V0, Tdebye, Vbar, rho)
omegaD = boltz*Tdebye/hbar;
mu = Vbar.^2*rho;
tao = (2*gamma^2*boltz*T*interpOmega.^2 ./ ...
    (mu*V0*omegaD)).^-1; % Eq. 13
% tao = (4*pi*gamma^2*boltz*T*interpOmega.^2 ./ ...
%     (mu*V0*omegaD)).^-1; % Eq. 13
end


%{
Average Velocity computed acording to Equation 6

@return Vbar: Average group velocity. Size: (1,50000)
@return interpVelo: interpolated velocity for each mode. Size: (N,50000)
@param kx: Phonon wave vector. Size: (1,xres)
@param omega: Phonon dispersion angular frequency. Size: (N,xres)
@param omega_vec: omega query for interpolation. Size: (1,50000)
%}
function [Vbar, interpVelo] = avg_velo(kx, omega, omega_vec)
N = size(omega, 1);
qlength = length(omega_vec);

Velo = gradient(omega, kx(2)-kx(1)); % m/s
interpVelo = zeros(N, qlength);
for n = 1:N
    interpVelo(n,:) = interp1(omega(n,:), Velo(n,:), omega_vec);
end
Vbar = mean(interpVelo.^-1, 1, 'omitnan').^-1; % Eq. 6
end


%{
Density of states computed according to Equation 7.

@param kx: wavevector in the x-direction. Shape: (1,xres)
@param omega: Phonon dispersion angular frequency. Size: (N,xres)
@param omega_vec: omega query for interpolation. Size: (1,50000)
@param V: interpolated velocity for each mode. Size: (N,50000)
@param t: heterostucture thickness, in m
%}
function gTot = density_states(kx, omega, omega_vec, V, t)
N = size(omega, 1);
qlength = length(omega_vec);

g = zeros(N, qlength);
for n = 1:N
    interpk = interp1(omega(n,:), kx, omega_vec);
    g(n,:) = interpk ./ V(n,:) ./ (2*pi*t); % Eq 7 (part 1)
end
gTot = sum(g, 1, 'omitnan'); % Eq. 7 (part 2)
end


%{
Function defined in Eq. 11

@param omega: Phonon angular frequency of any size
@param T: operational Temperature, in K
%}
function x = xfunction(omega, T)
x = hbar * omega / boltz / T;
end