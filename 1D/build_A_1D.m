%{
Build the matrix A for solving the eigenvalue problem [A]x=l[B]x

@param kx: Vector form of the wavevector
@param C: Vectorized form of the elastic coeficient matrix.
    Shape: (zres)x6x6
@param h: distance between nodes
@param zres: Number of discrete nodes in the z-direction
@return A: A matrix representation of the 1D continuum elastic wave
    equation
%}
function A = build_A_1D(kx, C, h, zres)
[p,q,r] = deal(zeros(length(C), 1));

for i = 1:length(C)
    % Apply boundary conditions
    im1 = i - 1;
    ip1 = i + 1;
    if i == 1
        im1 = 2;
    end
    if i == length(C)
        ip1 = length(C) - 1;
    end
    
    p(i) = (C(ip1,4,4)-C(im1,4,4))/(4*h^2) - C(i,4,4)/h^2;
    q(i) = C(i,4,4)*2/h^2 + C(i,6,6)*kx^2;
    r(i) = -(C(ip1,4,4)-C(im1,4,4))/(4*h^2) - C(i,4,4)/h^2;
end

diags = [p q r];
A = full(spdiags(diags, -1:1, zres, zres));
A(1,2) = p(1)+r(1);
A(end, end-1) = p(end)+r(end);
end