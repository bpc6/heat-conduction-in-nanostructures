clear
config;

% Analytic Solution
[N, kx, om_analytic] = analytic(thicknesses, materials, xres);
dispersionplot_1D(N, kx, om_analytic, 1)
title('Analytic Solution')

% Numeric Solution
[~, t, ~, om_numeric] = dispersion_1D(...
    thicknesses, materials, xres, zres);
dispersionplot_1D(N, kx, om_numeric, 2)
title('Numeric Solution')

% Thermal Conductivity
[kappa, ratio] = conductivity_1D(kx, om_numeric(2:N+1,:), t, 300, "GaN");