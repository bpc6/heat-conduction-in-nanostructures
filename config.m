% Array defining the thicknesses of each layer of the structure
thicknesses = [6]*1e-9;
% Array defining the materials for each layer of the structure
materials = ["GaN"];

% Resolution Parameters
xres = 150;
yres = 30;
zres = 150;

% Do you want to make a movie?
movie = false;