%% Error & Time vs zres, Heterostructure
clear
xres = 50;
yres = 2;
structure = [2 6 2];

% [N,kx,ky,omega_u_highres, omega_v_highres] = dispersion_2D(...
%     structure, ["AlN" "GaN" "AlN"], xres, yres, 801);
% load highres_2D.mat
% E_highres = omega_to_E(omega_u_highres(N+1,1));
% %%
% zres_vec = [30:10:100 150:50:300];
% error = zeros(length(zres_vec), 1);
% runtime = zeros(size(error));
% for i = 1:length(zres_vec)
%     fprintf('i = %d/%d\n', i, length(zres_vec))
%     tic
%     [N,kx,ky,omega_u,omega_v] = dispersion_2D(...
%         [2 6 2], ["AlN" "GaN" "AlN"], xres, yres, zres_vec(i));
%     runtime(i) = toc;
%     E = omega_to_E(omega_u(N+1,1));
%     error(i) = abs(E_highres - E) ./ E_highres * 100;
% end

load validation_data_2D.mat

res_per_t = (zres_vec-1) ./ sum(structure);
figure(5), clf
set(gcf, 'units', 'inches', 'position', [1,2,14,6])
sgtitle('2D Error, Runtime vs. Resolution per Thickness')
subplot(211)
semilogy(res_per_t, error, 'k')
xlabel('Resolution per thickness (points/nm)')
ylabel('Max error (%)')
box on, grid on
subplot(212)
plot(zres_vec, runtime/xres/yres, 'k')
xlabel('Resolution (points/nm)')
ylabel('Runtime per computation (sec)')
box on, grid on
structure_text = sprintf('[AlN GaN AlN]\n%s', mat2str(structure));
text(10, 2.5, structure_text)
saveas(gcf, 'Validation/2D_figs/heterostucture_stats_vs_res.png')