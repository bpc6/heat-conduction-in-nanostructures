clear

%% Initial Error Function
xres = 50;
zres = 60;
t = 6;

% Analytic Solution
[N, kx, om_analytic] = analytic(t, "GaN", xres);
E_analytic = omega_to_E(om_analytic);
figure(1), clf, box on
plot(kx/1e9, E_analytic)
xlabel('k_x (nm^{-1})')
ylabel('Phonon Energy (meV)')
title('Analytic Phonon Dispersion')
text(1, 30, sprintf('xres = %d', xres))
saveas(gcf, 'Validation/1D_figs/analytic_disp.png')

% Numerical Solution
[~,~,~,om_numeric] = dispersion_1D(t, "GaN", xres, zres);
E_numeric = omega_to_E(om_numeric(1:N+1,:));
figure(2), clf, box on, hold on
plot(kx/1e9, E_numeric)
xlabel('k_x (nm^{-1})')
ylabel('Phonon Energy (meV)')
title('Numeric Phonon Dispersion')
text(1, 30, sprintf('xres = %d\nzres = %d', xres, zres))
plot(0, E_numeric(end, 1), 'ro', 'markersize', 8)
x = [.22 .14];
y = [.5 .34];
annotation('textarrow', x,y,'String', sprintf('Point with largest error'))
saveas(gcf, 'Validation/1D_figs/numeric_disp.png')

% Error vs kx
error0 = abs(E_analytic - E_numeric) ./ E_analytic * 100;
error0(isinf(error0)) = 0;
figure(3), clf, box on, hold on
plot(kx/1e9, error0)
xlabel('k_x (nm^{-1})')
ylabel('Error (%)')
title('Numerical Error as a function of k_x')
saveas(gcf, 'Validation/1D_figs/error_vs_kx.png')


% Error & Time vs. zres, Thinfilm
zres_vec1 = [10:10:100 150:50:300 400 500];
error1 = zeros(length(zres_vec1), 1);
runtime1 = zeros(size(error1));
for i = 1:length(zres_vec1)
    fprintf('i = %d/%d\n', i, length(zres_vec1))
    tic
    [~,~,~,omega] = dispersion_1D(t, "GaN", xres, zres_vec1(i));
    runtime1(i) = toc;
    E = omega_to_E(omega(N+1,1));
    error1(i) = abs(E_analytic(end,1) - E) ./ E_analytic(end,1) * 100;
end
%%
res_per_t = (zres_vec1-1) ./ t;
figure(4), clf
set(gcf, 'units', 'inches', 'position', [1,2,14,6])
sgtitle('1D Error, Runtime vs. z-Resolution')
subplot(211)
semilogy(res_per_t, error1, 'k')
xlabel('Resolution per thickness (points/nm)')
ylabel('Max error (%)')
box on, grid on
subplot(212)
plot(zres_vec1, runtime1/xres, 'k')
xlabel('z-Resolution (points)')
ylabel('Runtime per computation (sec)')
box on, grid on
% text(10, .25, sprintf('xres = %d', xres))
structure_text = sprintf('%d nm thin film', t);
text(10, .65, structure_text)
saveas(gcf, 'Validation/1D_figs/thinfilm_stats_vs_res.png')

%% Error & Time vs xres, Thin Film
xres_vec1 = 10:100:510;
error1x = zeros(length(xres_vec1), 1);
runtime1x = zeros(size(error1x));
for i = 1:length(xres_vec1)
    [N, ~, om_analytic] = analytic(t, "GaN", xres_vec1(i));
    fprintf('i = %d/%d\n', i, length(zres_vec1))
    tic
    [~,~,~,omega] = dispersion_1D(t, "GaN", xres_vec1(i), zres);
    runtime1x(i) = toc;
    E = omega_to_E(omega(N+1,1));
    error1x(i) = abs(E_analytic(end,1) - E) ./ E_analytic(end,1) * 100;
end
%%
figure(5), clf
set(gcf, 'units', 'inches', 'position', [1,2,14,6])
sgtitle('1D Error, Runtime vs. x-Resolution')
subplot(211)
plot(xres_vec1, error1x, 'k')
xlabel('x-Resolution (points)')
ylabel('Max error (%)')
box on, grid on
subplot(212)
plot(xres_vec1, runtime1x, 'k')
xlabel('x-Resolution (points)')
ylabel('Runtime per cycle (sec)')
box on, grid on
text(30, .9, sprintf('zres = %d', zres))
saveas(gcf, 'Validation/1D_figs/thinfilm_xstats_vs_res.png')


%% Error & Time vs zres, Heterostructure
xres = 2;
structure = [4 6 4];
[N,~,kx,omega_highres] = dispersion_1D(structure, ["AlN" "GaN" "AlN"], xres, 1401);
E_highres = omega_to_E(omega_highres(N+1,1));

zres_vec2 = [20:10:100 150:50:300 400 500 600 800];
error2 = zeros(length(zres_vec2), 1);
runtime2 = zeros(size(error2));
for i = 1:length(zres_vec2)
    fprintf('i = %d/%d\n', i, length(zres_vec2))
    tic
    [N,~,kx,omega] = dispersion_1D(structure, ["AlN" "GaN" "AlN"], ...
        xres, zres_vec2(i));
    runtime2(i) = toc;
    E = omega_to_E(omega(N+1,1));
    error2(i) = abs(E_highres - E) ./ E_highres * 100;
end

res_per_t = (zres_vec2-1) ./ sum(structure);
figure(6), clf
set(gcf, 'units', 'inches', 'position', [1,2,14,6])
sgtitle('1D Error, Runtime vs. Resolution')
subplot(211)
semilogy(res_per_t, error2, 'k')
xlabel('Resolution per thickness (points/nm)')
ylabel('Max error (%)')
box on, grid on
subplot(212)
plot(zres_vec2, runtime2/xres, 'k')
xlabel('Resolution (points)')
ylabel('Runtime per computation (sec)')
box on, grid on
% text(10, 1.75, sprintf('xres = %d', xres))
structure_text = sprintf('[AlN GaN AlN]\n%s', mat2str(structure));
text(10, 4, structure_text)
saveas(gcf, 'Validation/1D_figs/heterostucture_stats_vs_res.png')