clear
xres = 50;
yres = 2;
structure = [2 6 2];
% [N, kx, ky, u_highres, v_highres, w_highres] = dispersion_3D(...
%     structure, ["AlN" "GaN" "AlN"], 2, 2, 701);
% %%
% xres = 50;
% E_highres = omega_to_E(u_highres(N+1,1));
% zres_vec = [30:10:100 150:50:300];
% error = zeros(length(zres_vec), 1);
% runtime = zeros(size(error));
% for i = 1:length(zres_vec)
%     fprintf('i = %d/%d\n', i, length(zres_vec))
%     tic
%     [N,kx,ky,omega_u,omega_v,omega_w] = dispersion_3D(...
%         [2 6 2], ["AlN" "GaN" "AlN"], xres, yres, zres_vec(i));
%     runtime(i) = toc;
%     E = omega_to_E(omega_u(N+1,1));
%     error(i) = abs(E_highres - E) ./ E_highres * 100;
% end

load 3D/highres_3D.mat

res_per_t = (zres_vec-1) ./ 10;
figure(5), clf
set(gcf, 'units', 'inches', 'position', [1,2,14,6])
sgtitle('3D Error, Runtime vs. Resolution per Thickness')
subplot(211)
semilogy(res_per_t, error, 'k')
xlabel('Resolution per thickness (points/nm)')
ylabel('Max error (%)')
box on, grid on
subplot(212)
plot(zres_vec, runtime/xres/yres, 'k')
xlabel('Resolution (points)')
ylabel('Runtime per computation (sec)')
box on, grid on, hold on
structure_text = sprintf('[AlN GaN AlN]\n%s', mat2str(structure));
text(10, 8, structure_text)
saveas(gcf, 'Validation/3D_figs/heterostucture_stats_vs_res.png')

fit = polyfit(zres_vec, runtime/xres/yres, 3);
query = 1:400;
plot(query, polyval(fit, query));