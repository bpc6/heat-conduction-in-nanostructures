%{
Run this file from the command window:
add_material(filename, rho, a, c, structure, Td, gamma)

A value is required for all fields, but only filename, rho, a, and c are
necessary for computing phonon dispersion. Therefore, structure, Td, and
gamma are arbitrary unless running thermal conductivity computations.

@param filename: The name of the file containing the elastic coefficient
matrix in Pa. It can be .txt, .csv, .xlsx but must be formatted as:
<material_name>.<extension>
Example: "Si.txt", "AlN.xlsx"
@param rho: density in kg/m^2
@param a, c: Lattice parameters (in m) for hexagonal structures. Use the
same value for cubic.
@param structure: The structure of the file as a string. Example: "cubic"
@param Td: Debye temperature in K
@param gamma: Gruneisen Anharmonicity parameter, usally between 0.5 and 2
%}
function add_material(filename, rho, a, c, structure, Td, gamma)
C = readmatrix(filename);
material = split(filename, '.');
material = material(1);
vol = volume(a, c, structure);

save(sprintf('./properties/%s', material), "C", "rho", "a", "c", "vol", ...
    "Td", "gamma");
end


function vol = volume(a, c, structure)
if structure == "cubic"
    vol = a^3;
elseif structure == "hexagonal" || structure == "HCP"
    vol = 3*sqrt(3)/2 * a^2 * c;
else
    txt = 'Enter a valid crystal structure such as "cubic" or "hexagonal"';
    error(txt)
end
end