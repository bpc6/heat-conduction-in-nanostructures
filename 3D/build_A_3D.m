%{
Build the A matrix to set up the eigenvalue problem: A*x=lambda*B*x

@param kx: x-component of the wavevector
@param ky: y-component of the wavevector
@param C: 3D matrix containing the elastic coefficients for each node
@param h: distance between nodes
@param zres: computation resolution in z direction (number of discrete
    nodes
@return A: a (3*zres)x(3*zres) matrix coding the dynamics of the
    continuum elastic wave equation for 3 dimensions
%}
function A = build_A_3D(kx, ky, C, h, zres)
[p,q,r] = deal(zeros(3,3,length(C)));

for i = 1:length(C)
    % Apply boundary conditions
    im1 = i - 1;
    ip1 = i + 1;
    if i == 1
        im1 = 2;
    end
    if i == length(C)
        ip1 = length(C) - 1;
    end
    
    kz = 0;
    
    % Assign the diagonal vectors
    % x direction -------------------------------------------------
    p(i,1,1) = (C(ip1,5,5) - C(im1,5,5))/(4*h^2) - C(i,5,5)/h^2;
    q(i,1,1) = C(i,1,1)*kx^2 + C(i,1,5)*kx*kz + C(i,1,6)*kx*ky + ...
        C(i,6,1)*kx*ky + C(i,6,5)*ky*kz + C(i,6,6)*ky^2 + ...
        C(i,5,1)*kx*kz + C(i,5,5)*(2/h^2 - kz^2) + C(i,5,6)*kx*kz;
    r(i,1,1) = -(C(ip1,5,5) - C(im1,5,5))/(4*h^2) - C(i,5,5)/h^2;
    
    p(i,1,2) = (C(ip1,5,4) - C(im1,5,4))/(4*h^2) - C(i,5,4)/h^2;
    q(i,1,2) = C(i,1,2)*kx*ky + C(i,1,4)*kx*kz + C(i,1,6)*kx^2 + ...
        C(i,6,2)*ky^2 + C(i,6,4)*ky*kz + C(i,6,6)*kx*ky + ...
        C(i,5,2)*ky*kz + C(i,5,4)*(2/h^2 - kz^2) + C(i,5,6)*kx*kz;
    r(i,1,2) = -(C(ip1,5,4) - C(im1,5,4))/(4*h^2) - C(i,5,4)/h^2;
    
    p(i,1,3) = (C(ip1,5,3) - C(im1,5,3))/(4*h^2) - C(i,5,3)/h^2;
    q(i,1,3) = C(i,1,3)*kx*ky + C(i,1,4)*kx*ky + C(i,1,5)*kx^2 + ...
        C(i,6,3)*ky*ky + C(i,6,4)*ky^2 + C(i,6,5)*kx*ky + ...
        C(i,5,3)*(2/h^2 - kz^2) + C(i,5,4)*ky*kz + C(i,5,5)*kx*kz;
    r(i,1,3) = -(C(ip1,5,3) - C(im1,5,3))/(4*h^2) - C(i,5,3)/h^2;
    % v direction -------------------------------------------------
    p(i,2,1) = (C(ip1,4,5) - C(im1,4,5))/(4*h^2) - C(i,4,5)/h^2;
    q(i,2,1) = C(i,6,1)*kx^2 + C(i,6,5)*kx*kz + C(i,6,6)*kx*ky + ...
        C(i,2,1)*kx*ky + C(i,2,5)*ky*kz + C(i,2,6)*ky^2 + ...
        C(i,4,1)*kx*kz + C(i,4,5)*(2/h^2 - kz^2) + C(i,4,6)*kx*kz;
    r(i,2,1) = -(C(ip1,4,5) - C(im1,4,5))/(4*h^2) - C(i,4,5)/h^2;
    
    p(i,2,2) = (C(ip1,4,4) - C(im1,4,4))/(4*h^2) - C(i,4,4)/h^2;
    q(i,2,2) = C(i,6,2)*kx*ky + C(i,6,4)*kx*kz + C(i,6,6)*kx^2 + ...
        C(i,2,2)*ky^2 + C(i,2,4)*ky*kz + C(i,2,6)*kx*ky + ...
        C(i,4,2)*ky*kz + C(i,4,4)*(2/h^2 - kz^2) + C(i,4,6)*kx*kz;
    r(i,2,2) = -(C(ip1,4,4) - C(im1,4,4))/(4*h^2) - C(i,4,4)/h^2;
    
    p(i,2,3) = (C(ip1,4,3) - C(im1,4,3))/(4*h^2) - C(i,4,3)/h^2;
    q(i,2,3) = C(i,6,3)*kx*ky + C(i,6,4)*kx*ky + C(i,6,5)*kx^2 + ...
        C(i,2,3)*ky*ky + C(i,2,4)*ky^2 + C(i,2,5)*kx*ky + ...
        C(i,4,3)*(2/h^2 - kz^2) + C(i,4,4)*ky*kz + C(i,4,5)*kx*kz;
    r(i,2,3) = -(C(ip1,4,3) - C(im1,4,3))/(4*h^2) - C(i,4,3)/h^2;
    % w direction -------------------------------------------------
    p(i,3,1) = (C(ip1,3,5) - C(im1,3,5))/(4*h^2) - C(i,3,5)/h^2;
    q(i,3,1) = C(i,5,1)*kx^2 + C(i,5,5)*kx*kz + C(i,5,6)*kx*ky + ...
        C(i,4,1)*kx*ky + C(i,4,5)*ky*kz + C(i,4,6)*ky^2 + ...
        C(i,3,1)*kx*kz + C(i,3,5)*(2/h^2 - kz^2) + C(i,3,6)*kx*kz;
    r(i,3,1) = -(C(ip1,3,5) - C(im1,3,5))/(4*h^2) - C(i,3,5)/h^2;
    
    p(i,3,2) = (C(ip1,3,4) - C(im1,3,4))/(4*h^2) - C(i,3,4)/h^2;
    q(i,3,2) = C(i,5,2)*kx*ky + C(i,5,4)*kx*kz + C(i,5,6)*kx^2 + ...
        C(i,4,2)*ky^2 + C(i,4,4)*ky*kz + C(i,4,6)*kx*ky + ...
        C(i,3,2)*ky*kz + C(i,3,4)*(2/h^2 - kz^2) + C(i,3,6)*kx*kz;
    r(i,3,2) = -(C(ip1,3,4) - C(im1,3,4))/(4*h^2) - C(i,3,4)/h^2;
    
    p(i,3,3) = (C(ip1,3,3) - C(im1,3,3))/(4*h^2) - C(i,3,3)/h^2;
    q(i,3,3) = C(i,5,3)*kx*ky + C(i,5,4)*kx*ky + C(i,5,5)*kx^2 + ...
        C(i,4,3)*ky*ky + C(i,4,4)*ky^2 + C(i,4,5)*kx*ky + ...
        C(i,3,3)*(2/h^2 - kz^2) + C(i,3,4)*ky*kz + C(i,3,5)*kx*kz;
    r(i,3,3) = -(C(ip1,3,3) - C(im1,3,3))/(4*h^2) - C(i,3,3)/h^2;
end

% Build 9 sub matrices using the diagonals above
A = zeros(3*zres);
for ii = 1:3
    for jj = 1:3
        diags = [p(:,ii,jj) q(:,ii,jj) r(:,ii,jj)];
        A1 = full(spdiags(diags, -1:1, zres, zres));
        A1(1,2) = p(1,ii,jj)+r(1,ii,jj);
        A1(end, end-1) = p(end,ii,jj)+r(end,ii,jj);
        A((ii-1)*zres+1:ii*zres, (jj-1)*zres+1:jj*zres) = A1;
    end
end
end