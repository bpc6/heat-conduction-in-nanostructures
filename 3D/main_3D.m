clear
config;

[N, t, kx, ky, omega_u, omega_v, omega_w] = dispersion_3D(...
    thicknesses, materials, xres, yres, zres);

dispersionplot_3D(N, t*1e9, kx, ky, omega_u, omega_v, omega_w, movie)